package com.itswa.microservicios.app.cursos.models.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.itswa.microservicios.app.commons.alumnos.entities.Alumno;
import com.itswa.microservicios.app.cursos.models.entity.Curso;
import com.itswa.microservicios.app.cursos.models.services.CursoService;
import com.itswa.microservicios.commons.controllers.CommonController;
import com.itswa.microservicios.commons.examenes.entity.Examen;

@RestController
public class CursoController extends CommonController<Curso, CursoService> {
	
	@PutMapping("/{id}")
	 public ResponseEntity<?> editar(@RequestBody Curso curso, @PathVariable Long id){
		 Optional<Curso> optional = service.findById(id);
		 if(optional.isEmpty()) {
			return ResponseEntity.notFound().build();
		 }
		 Curso cursoDB = optional.get();
		 cursoDB.setName(curso.getName());

		 
		 return ResponseEntity.status(HttpStatus.CREATED).body(service.save(cursoDB));
	 }

	@PutMapping("/{id}/asignar-alumnos")
	 public ResponseEntity<?> asignarAlumnos(@RequestBody List<Alumno> alumnos, @PathVariable Long id){
		 Optional<Curso> optional = service.findById(id);
		 if(optional.isEmpty()) {
			return ResponseEntity.notFound().build();
		 }
		 Curso cursoDB = optional.get();
		 alumnos.forEach( a -> {
			 cursoDB.addAlumno(a);
		 });
		 
		 return ResponseEntity.status(HttpStatus.CREATED).body(service.save(cursoDB));
	 }
	
	@PutMapping("/{id}/eliminar-alumno")
	 public ResponseEntity<?> eliminarAlumno(@RequestBody Alumno alumno, @PathVariable Long id){
		 Optional<Curso> optional = service.findById(id);
		 if(optional.isEmpty()) {
			return ResponseEntity.notFound().build();
		 }
		 Curso cursoDB = optional.get();

		 cursoDB.removeAlumno(alumno);
		 
		 return ResponseEntity.status(HttpStatus.CREATED).body(service.save(cursoDB));
	 }
	
	@GetMapping("alumno/{id}")
	 public ResponseEntity<?> buscarPorAlumnoId(@PathVariable Long id){
		return ResponseEntity.ok(service.findCursoByAlumnoId(id));
	}
	
	@PutMapping("/{id}/asignar-examenes")
	 public ResponseEntity<?> asignarExamenes(@RequestBody List<Examen> examenes, @PathVariable Long id){
		 Optional<Curso> optional = service.findById(id);
		 if(optional.isEmpty()) {
			return ResponseEntity.notFound().build();
		 }
		 Curso cursoDB = optional.get();
		 examenes.forEach( a -> {
			 cursoDB.addExamen(a);
		 });
		 
		 return ResponseEntity.status(HttpStatus.CREATED).body(service.save(cursoDB));
	 }
	
	@PutMapping("/{id}/eliminar-examen")
	 public ResponseEntity<?> eliminarExamen(@RequestBody Examen examen, @PathVariable Long id){
		 Optional<Curso> optional = service.findById(id);
		 if(optional.isEmpty()) {
			return ResponseEntity.notFound().build();
		 }
		 Curso cursoDB = optional.get();

		 cursoDB.removeExamen(examen);
		 
		 return ResponseEntity.status(HttpStatus.CREATED).body(service.save(cursoDB));
	 }
}
