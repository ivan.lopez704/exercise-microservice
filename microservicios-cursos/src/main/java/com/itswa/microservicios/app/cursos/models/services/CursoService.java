package com.itswa.microservicios.app.cursos.models.services;

import com.itswa.microservicios.app.cursos.models.entity.Curso;
import com.itswa.microservicios.commons.services.CommonService;

public interface CursoService extends CommonService<Curso> {

	public Curso findCursoByAlumnoId(Long id);
}
