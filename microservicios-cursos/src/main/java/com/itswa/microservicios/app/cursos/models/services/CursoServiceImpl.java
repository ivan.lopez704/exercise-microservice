package com.itswa.microservicios.app.cursos.models.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itswa.microservicios.app.cursos.models.entity.Curso;
import com.itswa.microservicios.app.cursos.models.respository.CursoRepository;
import com.itswa.microservicios.commons.services.CommonServiceImpl;

@Service
public class CursoServiceImpl extends CommonServiceImpl<Curso, CursoRepository> implements CursoService {

	@Override
	@Transactional(readOnly = true)
	public Curso findCursoByAlumnoId(Long id) {
		return repository.findCursoByAlumnoId(id);
	}


}
