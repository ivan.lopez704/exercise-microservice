package com.itswa.microservicios.app.examenes.service;

import java.util.List;

import com.itswa.microservicios.commons.examenes.entity.Examen;
import com.itswa.microservicios.commons.services.CommonService;

public interface ExamenService extends CommonService<Examen> {

	public List<Examen> findByNombre(String term);
}
