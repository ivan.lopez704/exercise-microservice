package com.itswa.microservicios.app.usuarios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
@EntityScan({"com.itswa.microservicios.app.commons.alumnos.entities"})
public class MicroserviciosUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviciosUserApplication.class, args);
	}

}
