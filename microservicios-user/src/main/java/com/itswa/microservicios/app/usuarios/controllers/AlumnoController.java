package com.itswa.microservicios.app.usuarios.controllers;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.itswa.microservicios.app.commons.alumnos.entities.Alumno;
import com.itswa.microservicios.app.usuarios.service.AlumnoService;
import com.itswa.microservicios.commons.controllers.CommonController;

@RestController
public class AlumnoController extends CommonController<Alumno, AlumnoService> {
	 
	 @PostMapping("/{id}")
	 public ResponseEntity<?> editar(@RequestBody Alumno alumno, @PathVariable Long id){
		 Optional<Alumno> optional = service.findById(id);
		 if(optional.isEmpty()) {
			return ResponseEntity.notFound().build();
		 }
		 Alumno alumnoDB = optional.get();
		 alumnoDB.setNombre(alumno.getNombre());
		 alumnoDB.setApellido(alumno.getApellido());
		 alumnoDB.setEmail(alumno.getEmail());
		 
		 return ResponseEntity.status(HttpStatus.CREATED).body(service.save(alumnoDB));
	 }
	 
	 @GetMapping("/filtrar/{term}")
	 public ResponseEntity<?> filtrar(@PathVariable String term){
		 return ResponseEntity.ok(service.findByNombreOrApellido(term));
	 }
}
