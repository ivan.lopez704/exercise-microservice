package com.itswa.microservicios.app.usuarios.service;

import java.util.List;

import com.itswa.microservicios.app.commons.alumnos.entities.Alumno;
import com.itswa.microservicios.commons.services.CommonService;

public interface AlumnoService extends CommonService<Alumno> {

	public List<Alumno> findByNombreOrApellido(String term);
}
